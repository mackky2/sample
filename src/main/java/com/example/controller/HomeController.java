package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.entity.Customer;

@Controller
public class HomeController {
    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

	@RequestMapping(value = "")
	public String index(Model model) {
		
		String sql = "SELECT id, first_name, last_name FROM customers WHERE id = :id";
		SqlParameterSource param = new MapSqlParameterSource()
				.addValue("id", 1);
		Customer result = jdbcTemplate.
				queryForObject(sql, param,
				(rs, rowNum) -> new Customer(rs.getInt("id"), 
						rs.getString("first_name"), rs.getString("last_name"))	);
		model.addAttribute("test", result.getLastName() + result.getFirstName());
		return "index";
	}
}
